import {LightningElement, api, wire, track} from "lwc";
import {refreshApex} from "@salesforce/apex";
import {getRecord} from "lightning/uiRecordApi";
import {qrcodegen} from "c/qrcodegen";

/* Example VCard formula:
"BEGIN:VCARD"
+ BR() + "VERSION:3.0"
+ BR() + "N:" + LastName + ";" + FirstName + ";;" + TEXT(Salutation) + ";"
+ BR() + "FN:" + FirstName + " " + LastName
+ BR() + "BDAY:" + TEXT(YEAR(Birthdate)) + TEXT(MONTH(Birthdate)) + TEXT(DAY(Birthdate))
+ BR() + "TITLE:" + Title
+ BR() + "TEL:" + Phone
+ BR() + "EMAIL:" + Email
+ BR() + "ORG:" + Account.Name
+ BR() + "END:VCARD"
*/

export default class QrGenerator extends LightningElement
{
	@api recordId;
	@api objectApiName;
	@api title = "QR";
	@api
	get fieldName()
	{
		return this._fieldName;
	}
	set fieldName(fn)
	{
		this._fieldName = fn;
		if (this.record)
			this.text = this.record[fn].value;
	}
	@api
	get text()
	{
		return this._text;
	}
	set text(value)
	{
		if (this.replaceBR)
			value = value.replace(/<br\/?>/ig, "\r\n");
		this._text = value;
	}
	@api scale = 8; // pixels per module
	@api errCorLvl = "MEDIUM";

	@api replaceBR = false;

	@track _text = "";
	@track stats = "";
	@track fields = [];

	@track errorMsg;

	set record(rec)
	{
		this._record = rec;
		if (this.fieldName && this._record[this.fieldName])
			this.text = this._record[this.fieldName].value;
	}
	get record()
	{
		return this._record;
	}

	connectedCallback()
	{
		this.fields = [this.objectApiName + "." + this.fieldName];
	}

	@wire(getRecord, {recordId: "$recordId", fields: "$fields"})
	getRecordCallback({data, error})
	{
		if (error)
		{
			console.error(error);
			this.errorMsg = error && error.body && error.body.message;
		}
		else
		{
			if (data && data.fields)
				this.record = data.fields;
		}
	}

	renderedCallback()
	{
		try
		{
			if (this.text)
				this.redrawQrCode();
		}
		catch (err)
		{
			console.error(err);
		}
	}

	doRefresh()
	{
		refreshApex(this.getRecordCallback);
	}


	redrawQrCode()
	{
		// Reset output images in case of early termination
		let canvas = this.template.querySelector("canvas.qrcode-canvas");
		canvas.style.display = "none";
		// Returns a QrCode.Ecc object based on the radio buttons in the HTML form.
		function getInputErrorCorrectionLevel(errCorLvl)
		{
			if (errCorLvl === "MEDIUM")
				return qrcodegen.QrCode.Ecc.MEDIUM;
			else if (errCorLvl === "QUARTILE")
				return qrcodegen.QrCode.Ecc.QUARTILE;
			else if (errCorLvl === "HIGH")
				return qrcodegen.QrCode.Ecc.HIGH;
			// In case no radio button is depressed
			return qrcodegen.QrCode.Ecc.LOW;
		}

		// Get form inputs and compute QR Code
		let ecl = getInputErrorCorrectionLevel(this.errCorLvl);
		let text = this.text;
		let segs = qrcodegen.QrSegment.makeSegments(text);
		let minVer = 1;
		let maxVer = 40;
		let mask = -1; // TODO maybe make option for this?
		let boostEcc = true;
		let qr = qrcodegen.QrCode.encodeSegments(segs, ecl, minVer, maxVer, mask, boostEcc);

		// Draw image output
		let border = 4;
		if (border < 0 || border > 100)
			return;
		let scale = this.scale;
		if (scale <= 0 || scale > 30)
			return;
		qr.drawCanvas(scale, border, canvas);
		canvas.style.removeProperty("display");

		// Returns a string to describe the given list of segments.
		function describeSegments(segs)
		{
			if (segs.length === 0)
				return "none";
			else if (segs.length === 1)
			{
				let mode = segs[0].mode;
				let Mode = qrcodegen.QrSegment.Mode;
				if (mode === Mode.NUMERIC     )  return "numeric";
				if (mode === Mode.ALPHANUMERIC)  return "alphanumeric";
				if (mode === Mode.BYTE        )  return "byte";
				if (mode === Mode.KANJI       )  return "kanji";
				return "unknown";
			}
			return "multiple";
		}

		// Returns the number of Unicode code points in the given UTF-16 string.
		function countUnicodeChars(str)
		{
			let result = 0;
			for (let i = 0; i < str.length; i++, result++)
			{
				let c = str.charCodeAt(i);
				if (c < 0xD800 || c >= 0xE000)
					continue;
				else if (0xD800 <= c && c < 0xDC00 && i + 1 < str.length) {  // High surrogate
					i++;
					let d = str.charCodeAt(i);
					if (0xDC00 <= d && d < 0xE000)  // Low surrogate
						continue;
				}
				throw new Error("Invalid UTF-16 string");
			}
			return result;
		}

		// Show the QR Code symbol"s statistics as a string
		let stats = "QR Code version = " + qr.version + ", ";
		stats += "mask pattern = " + qr.mask + ", ";
		stats += "character count = " + countUnicodeChars(text) + ",\n";
		stats += "encoding mode = " + describeSegments(segs) + ", ";
		stats += "error correction = level " + "LMQH".charAt(qr.errorCorrectionLevel.ordinal) + ", ";
		stats += "data bits = " + qrcodegen.QrSegment.getTotalBits(segs, qr.version) + ".";
		this.stats = stats;
	}
}